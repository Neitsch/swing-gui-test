/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.recorder.exception;

import com.schuster.exception.GuiTestException;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 6, 2015
 */
public class RecorderException extends GuiTestException {
  /**
   * @author nschuste
   * @version 1.0.0
   * @param constructMessage
   * @since Aug 6, 2015
   */
  public RecorderException(final String constructMessage) {
    super(constructMessage);
  }
}
