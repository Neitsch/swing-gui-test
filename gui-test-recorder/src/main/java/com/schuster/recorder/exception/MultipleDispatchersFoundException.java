/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.recorder.exception;

import java.awt.AWTEvent;

import com.schuster.common.extendable.EventDispatcher;
import com.schuster.exception.DispatchException;
import com.schuster.util.CommonConstants;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class MultipleDispatchersFoundException extends DispatchException {

  /**
   * @author nschuste
   * @version 1.0.0
   * @param eventDispatcher
   * @param e
   * @since Aug 5, 2015
   */
  public MultipleDispatchersFoundException(final AWTEvent event,
      final EventDispatcher... eventDispatchers) {
    super(constructMessage(event, eventDispatchers));
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @param eventDispatchers
   * @return
   * @since Aug 5, 2015
   */
  private static String constructMessage(final AWTEvent event,
      final EventDispatcher[] eventDispatchers) {
    final StringBuilder result = new StringBuilder();
    result.append(CommonConstants.LINE_SEPARATOR);
    result.append("Found more than one Dispatcher for Event: ");
    result.append(CommonConstants.LINE_SEPARATOR);
    result.append(event.toString());
    result.append(CommonConstants.LINE_SEPARATOR);
    result.append(CommonConstants.LINE_SEPARATOR);
    result.append("Got the following Dispatchers:");
    result.append(CommonConstants.LINE_SEPARATOR);
    for (final EventDispatcher edp : eventDispatchers) {
      result.append(edp.getBaseInfo().getToolName());
      result.append(CommonConstants.LINE_SEPARATOR);
    }
    return result.toString();
  }
}
