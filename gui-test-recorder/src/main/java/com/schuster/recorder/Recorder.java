/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.recorder;

import java.awt.AWTEvent;
import java.util.Iterator;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.schuster.capture.Capture;
import com.schuster.capture.Handler;
import com.schuster.common.extendable.EventDispatcher;
import com.schuster.common.registries.AbstractRegistry;
import com.schuster.exception.DispatchException;
import com.schuster.recorder.exception.MultipleDispatchersFoundException;
import com.schuster.util.Pair;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class Recorder implements Handler {
  private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);
  private Capture capture;
  private final AbstractRegistry<EventDispatcher> reg;

  /**
   * @author nschuste
   * @version 1.0.0
   * @since Aug 5, 2015
   */
  public Recorder(final AbstractRegistry<EventDispatcher> reg) {
    LOGGER.trace("Initializing Recorder");
    this.reg = reg;
    LOGGER.trace("Initialized Recorder");
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.capture.Handler#dispatch(java.awt.AWTEvent)
   * @since Aug 5, 2015
   */
  @Override
  public void dispatch(final AWTEvent event) throws DispatchException {
    LOGGER.debug(String.format("Trying to dispatch event: %s", event.paramString()));
    try {
      final Iterator<EventDispatcher> iter = this.reg.getIter();
      Optional<EventDispatcher> dispatcher = Optional.empty();
      while (iter.hasNext()) {
        final EventDispatcher e = iter.next();
        if (e.canDispatch(event)) {
          if (dispatcher.isPresent()) {
            throw new MultipleDispatchersFoundException(event, e, dispatcher.get());
          }
          dispatcher = Optional.of(e);
        }
      }
      if (dispatcher.isPresent()) {
        final Pair<String, String[]> call = dispatcher.get().getCallInfo(event);
        LOGGER.debug(String.format("Got call information: %s", call.toString()));
      }
    } catch (final Exception e) {
      LOGGER.info("Problem dispatching Event.", e);
    }
  }

  public void startRecording() {
    LOGGER.debug("Going to start recording.");
    this.capture = Capture.initialize(this);
    LOGGER.info("Started recording.");
  }
}
