/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.util;

import java.util.Optional;

import org.assertj.swing.core.BasicRobot;
import org.assertj.swing.core.Robot;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class SwingContext {
  private static volatile Optional<Robot> robot = Optional.empty();

  public static Robot getSwingRobot() {
    synchronized (robot) {
      if (!robot.isPresent()) {
        robot = Optional.of(BasicRobot.robotWithCurrentAwtHierarchy());
      }
      return robot.get();
    }
  }
}
