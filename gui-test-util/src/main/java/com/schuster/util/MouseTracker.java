/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.util;

import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Window;
import java.util.Optional;

import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class MouseTracker {
  private static Component currentComp = null;
  private static final Logger LOGGER = LoggerFactory.getLogger(MouseTracker.class);

  /**
   * @author nschuste
   * @version 1.0.0
   * @since Aug 5, 2015
   */
  public MouseTracker() {}

  public static Optional<Component> getComponent() {
    final Component c = findComponentUnderMouse();
    if (c != currentComp) {
      LOGGER.debug(String.format("Moved from Component %s to %s", currentComp, c));
      currentComp = c;
    }
    return Optional.ofNullable(c);
  }

  private static Component findComponentUnderMouse() {
    try {
      final Window window = findWindow();
      final Point location = MouseInfo.getPointerInfo().getLocation();
      SwingUtilities.convertPointFromScreen(location, window);
      return SwingUtilities.getDeepestComponentAt(window, location.x, location.y);
    } catch (final Exception e) {
      return null;
    }
  }

  private static Window findWindow() {
    for (final Window window : Window.getWindows()) {
      if (window.getMousePosition(true) != null) {
        return window;
      }
    }
    return null;
  }
}
