/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.util;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class Pair<K, V> {
  public K i;
  public V ii;

  /**
   * @author nschuste
   * @version 1.0.0
   * @since Aug 6, 2015
   */
  public Pair() {}

  /**
   * @author nschuste
   * @version 1.0.0
   * @param string
   * @param string2
   * @since Aug 5, 2015
   */
  public Pair(final K k, final V v) {
    this.i = k;
    this.ii = v;
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see java.lang.Object#toString()
   * @since Aug 5, 2015
   */
  @Override
  public String toString() {
    return String.format("%s[Key: %s, Value: %s]", this.getClass().getName(), this.i.toString(),
        this.ii.toString());
  }
}
