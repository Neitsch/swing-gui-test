/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.util;

import java.awt.Component;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 6, 2015
 */
public class NameConstructor {

  /**
   * @author nschuste
   * @version 1.0.0
   * @param target
   * @return
   * @since Aug 6, 2015
   */
  public static String constructFromComponent(final Component target) {
    return target.getName();
  }

}
