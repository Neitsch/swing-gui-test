/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.util;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class CommonConstants {
  public static final String LINE_SEPARATOR = System.getProperty("line.separator");
}
