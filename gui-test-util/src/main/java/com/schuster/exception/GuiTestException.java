/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.exception;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class GuiTestException extends Exception {

  /**
   * @author nschuste
   * @version 1.0.0
   * @param constructMessage
   * @since Aug 5, 2015
   */
  public GuiTestException(final String constructMessage) {}
}
