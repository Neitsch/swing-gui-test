/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.exception;



/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 6, 2015
 */
public class UnsupportedException extends GuiTestException {
  /**
   * @author nschuste
   * @version 1.0.0
   * @param constructMessage
   * @since Aug 6, 2015
   */
  public UnsupportedException(final String constructMessage) {
    super(constructMessage);
  }
}
