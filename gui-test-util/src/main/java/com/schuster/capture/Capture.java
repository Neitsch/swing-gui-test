/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.capture;

import java.awt.AWTEvent;
import java.awt.Toolkit;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class Capture {
  public static final long AWT_EVENT_MASK = AWTEvent.ACTION_EVENT_MASK
      + AWTEvent.ADJUSTMENT_EVENT_MASK + AWTEvent.COMPONENT_EVENT_MASK
      + AWTEvent.CONTAINER_EVENT_MASK + AWTEvent.FOCUS_EVENT_MASK
      + AWTEvent.HIERARCHY_BOUNDS_EVENT_MASK + AWTEvent.HIERARCHY_EVENT_MASK
      + AWTEvent.INPUT_METHOD_EVENT_MASK + AWTEvent.INVOCATION_EVENT_MASK
      + AWTEvent.ITEM_EVENT_MASK + AWTEvent.KEY_EVENT_MASK + AWTEvent.MOUSE_EVENT_MASK
      + AWTEvent.MOUSE_MOTION_EVENT_MASK + AWTEvent.MOUSE_WHEEL_EVENT_MASK
      + AWTEvent.PAINT_EVENT_MASK + AWTEvent.TEXT_EVENT_MASK + AWTEvent.WINDOW_EVENT_MASK
      + AWTEvent.WINDOW_FOCUS_EVENT_MASK + AWTEvent.WINDOW_STATE_EVENT_MASK;

  public static Capture initialize(final Handler eventHandler) {
    Toolkit.getDefaultToolkit()
        .addAWTEventListener(new EventListener(eventHandler), AWT_EVENT_MASK);
    return new Capture();
  }
}
