/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.capture;

import java.awt.AWTEvent;

import com.schuster.exception.DispatchException;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public interface Handler {

  /**
   * @author nschuste
   * @version 1.0.0
   * @param event
   * @throws DispatchException
   * @since Aug 5, 2015
   */
  void dispatch(AWTEvent event) throws DispatchException;

}
