/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.capture;

import java.awt.AWTEvent;
import java.awt.event.AWTEventListener;

import com.schuster.exception.DispatchException;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class EventListener implements AWTEventListener {
  private final Handler handler;

  /**
   * @author nschuste
   * @version 1.0.0
   * @param eventHandler
   * @since Aug 5, 2015
   */
  public EventListener(final Handler eventHandler) {
    super();
    this.handler = eventHandler;
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see java.awt.event.AWTEventListener#eventDispatched(java.awt.AWTEvent)
   * @since Aug 5, 2015
   */
  @Override
  public void eventDispatched(final AWTEvent event) {
    try {
      this.handler.dispatch(event);
    } catch (final DispatchException e) {
      e.printStackTrace();
    }
  }
}
