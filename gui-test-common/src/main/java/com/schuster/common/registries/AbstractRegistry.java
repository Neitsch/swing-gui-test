/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.common.registries;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class AbstractRegistry<T> {
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRegistry.class);
  private final Collection<T> components;

  /**
   * @author nschuste
   * @version 1.0.0
   * @since Aug 5, 2015
   */
  public AbstractRegistry() {
    LOGGER.trace("Initializing AbstractRegistry");
    this.components = new LinkedList<T>();
    LOGGER.trace("Initialized AbstractRegistry");
  }

  public Iterator<T> getIter() {
    return this.components.iterator();
  }

  public void register(final T me) {
    this.components.add(me);
    LOGGER.trace(String.format("Registered Component: %s", me.toString()));
  }
}
