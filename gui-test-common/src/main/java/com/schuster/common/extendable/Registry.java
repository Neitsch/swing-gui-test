/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.common.extendable;

import com.schuster.common.registries.AbstractRegistry;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public interface Registry {
  BaseInformation getInfo();

  void registerDispatcher(AbstractRegistry<EventDispatcher> dispatcherRegistry);

  void registerTask(AbstractRegistry<TaskBuilder> taskRegistry);
}
