/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.common.extendable;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public interface BaseInformation {

  String getToolId();

  /**
   * @author nschuste
   * @version 1.0.0
   * @return
   * @since Aug 5, 2015
   */
  String getToolName();
}
