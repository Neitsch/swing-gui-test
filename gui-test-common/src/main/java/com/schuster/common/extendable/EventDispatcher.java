/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.common.extendable;

import java.awt.AWTEvent;

import com.schuster.exception.UnsupportedException;
import com.schuster.util.Pair;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public interface EventDispatcher {

  /**
   * @author nschuste
   * @version 1.0.0
   * @param event
   * @return
   * @since Aug 5, 2015
   */
  boolean canDispatch(AWTEvent event);

  /**
   * @author nschuste
   * @version 1.0.0
   * @return
   * @since Aug 5, 2015
   */
  BaseInformation getBaseInfo();

  /**
   * @author nschuste
   * @version 1.0.0
   * @return
   * @throws UnsupportedException
   * @since Aug 6, 2015
   */
  Pair<String, String[]> getCallInfo(AWTEvent event) throws UnsupportedException;

}
