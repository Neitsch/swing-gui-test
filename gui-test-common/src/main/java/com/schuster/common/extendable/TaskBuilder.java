/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.common.extendable;

import java.util.concurrent.Callable;

import com.schuster.common.helper.Result;
import com.schuster.exception.UnsupportedException;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 6, 2015
 */
public interface TaskBuilder {
  Callable<Result> buildTask(String taskName, String[] parameters) throws UnsupportedException;

  boolean supports(String taskName);
}
