/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 7, 2015
 */
@SpringBootApplication
public class Application {
  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
