/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.web;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 7, 2015
 */
@RestController
public class GreetingController {

  private static final String template = "%s: Hello, %s!";
  private final AtomicLong counter = new AtomicLong();

  @RequestMapping("/greeting")
  public String greeting(@RequestParam(value = "name", defaultValue = "World") final String name) {
    return new String(String.format(template, this.counter.incrementAndGet(), name));
  }
}
