/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.schuster.common.extendable.EventDispatcher;
import com.schuster.common.extendable.Registry;
import com.schuster.common.registries.AbstractRegistry;
import com.schuster.gui.RegistryController;
import com.schuster.recorder.Recorder;
import com.schuster.widgettracker.WidgetTracker;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 6, 2015
 */
public class Launcher {
  public static Launcher launcher;
  private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);
  private final Recorder recorder;
  private final WidgetTracker tracker;

  /**
   * @author nschuste
   * @version 1.0.0
   * @since Aug 6, 2015
   */
  public Launcher() {
    LOGGER.trace("Initializing Launcher");
    this.tracker = new WidgetTracker();
    final AbstractRegistry<EventDispatcher> reg = new AbstractRegistry<>();
    final Registry registry = new RegistryController();
    registry.registerDispatcher(reg);
    this.recorder = new Recorder(reg);
    this.recorder.startRecording();
    LOGGER.trace("Initialized Launcher");
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @param args
   * @since Aug 6, 2015
   */
  public static void main(final String[] args) {
    LOGGER.debug("Main Thread Launched");
    LOGGER.debug("Args: " + Arrays.toString(args));
    launcher = new Launcher();
  }
}
