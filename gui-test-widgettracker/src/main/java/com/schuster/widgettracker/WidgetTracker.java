/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.widgettracker;

import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JLabel;

import com.schuster.util.MouseTracker;
import com.schuster.util.Pair;
import com.schuster.widgettracker.support.ComponentInformation;
import com.schuster.widgettracker.support.ComponentInformationBuilder;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class WidgetTracker {
  private class Refresh implements Runnable {
    MouseTracker tracker;

    /**
     * @author nschuste
     * @version 1.0.0
     * @since Aug 5, 2015
     */
    public Refresh(final MouseTracker tr) {
      this.tracker = tr;
    }

    /**
     * {@inheritDoc}
     *
     * @author nschuste
     * @version 1.0.0
     * @see java.lang.Runnable#run()
     * @since Aug 5, 2015
     */
    @Override
    public void run() {
      try {
        final ComponentInformation compInf =
            new ComponentInformationBuilder().component(MouseTracker.getComponent()).build();
        WidgetTracker.this.update(compInf);
      } catch (final Exception e) {
      } finally {
      }
    }
  }

  private static final String[] values = new String[] {"Class", "Name", "Unique"};
  private Map<String, Pair<JLabel, JLabel>> displays;
  private JFrame frame;
  private Refresh refTask;

  /**
   * Create the application.
   */
  public WidgetTracker() {
    java.awt.EventQueue.invokeLater(() -> {
      this.initialize();
      this.frame.setVisible(true);
    });
  }

  public void update(final ComponentInformation info) {
    for (final String string : values) {
      this.displays.get(string).i.setText(string);
      this.displays.get(string).ii.setText(info.info.get(string));
    }
    java.awt.EventQueue.invokeLater(() -> this.frame.validate());
    java.awt.EventQueue.invokeLater(this.refTask);
  }

  /**
   * Initialize the contents of the frame.
   *
   * @wbp.parser.entryPoint
   */
  private void initialize() {
    this.frame = new JFrame();
    this.frame.setBounds(100, 100, 450, 300);
    this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.frame.getContentPane().setLayout(null);

    this.displays = new TreeMap<>();
    int count = 0;
    for (final String string : values) {
      final JLabel key = new JLabel();
      final JLabel val = new JLabel();
      key.setBounds(0, 20 * count, 200, 20);
      val.setBounds(200, 20 * count, 200, 20);
      this.frame.getContentPane().add(key);
      this.frame.getContentPane().add(val);
      this.displays.put(string, new Pair(key, val));
      count++;
    }

    final MouseTracker tracker = new MouseTracker();
    this.refTask = new Refresh(tracker);
    java.awt.EventQueue.invokeLater(this.refTask);
  }
}
