/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.widgettracker.support;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class ComponentInformation {
  public Map<String, String> info = new TreeMap<>();
}
