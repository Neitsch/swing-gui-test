/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.widgettracker.support;

import java.awt.Component;
import java.util.Optional;

import com.schuster.util.SwingContext;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class ComponentInformationBuilder {
  private Optional<Component> comp;

  public ComponentInformation build() {
    final ComponentInformation info = new ComponentInformation();
    if (this.comp.isPresent()) {
      info.info.put("Class", this.comp.get().getClass().toString());
      info.info.put("Name", this.comp.get().getName());
      boolean unique;
      try {
        SwingContext.getSwingRobot().finder().findByName(info.info.get("Name"));
        unique = true;
      } catch (final Exception e) {
        unique = false;
      }
      info.info.put("Unique", Boolean.toString(unique));
    }
    return info;
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @param target
   * @return
   * @since Aug 5, 2015
   */
  public ComponentInformationBuilder component(final Optional<Component> target) {
    this.comp = target;
    return this;
  }
}
