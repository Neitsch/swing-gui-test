/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.gui.internal;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.schuster.common.helper.Result;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 6, 2015
 */
public class MouseCall implements Callable<Result> {
  public static final String CALL_NAME = "mouseclick";
  private static final Logger LOGGER = LoggerFactory.getLogger(MouseCall.class);
  private final int button;
  private final String componentName;
  private final int modifiers;

  private MouseCall(final String componentName, final int modifiers, final int button) {
    this.componentName = componentName;
    this.modifiers = modifiers;
    this.button = button;
    LOGGER.debug(String.format(
        "Initialized MouseCall with arguments:[componentName=%s,modifiers=%s,button=%s]",
        componentName, modifiers, button));
  }

  public static MouseCall constructFromMouseEvent(final MouseEvent e) {
    final Component target = com.schuster.util.MouseTracker.getComponent().get();
    final MouseCall call =
        new MouseCall(com.schuster.util.NameConstructor.constructFromComponent(target),
            e.getModifiers(), e.getButton());
    LOGGER.info(String.format("Created MouseCall from event: %s", e.paramString()));
    return call;
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @param parameters
   * @return
   * @since Aug 6, 2015
   */
  public static MouseCall withParameters(final String[] parameters) {
    return new MouseCall(parameters[0], Integer.parseInt(parameters[1]),
        Integer.parseInt(parameters[2]));
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see java.util.concurrent.Callable#call()
   * @since Aug 6, 2015
   */
  @Override
  public Result call() throws Exception {
    return null;
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @return
   * @since Aug 6, 2015
   */
  public String getId() {
    return CALL_NAME;
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @return
   * @since Aug 6, 2015
   */
  public String[] getStringParameters() {
    return new String[] {this.componentName, Integer.toString(this.modifiers),
        Integer.toString(this.button)};
  }
}
