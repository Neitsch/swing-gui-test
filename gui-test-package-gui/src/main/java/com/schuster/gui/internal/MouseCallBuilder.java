/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.gui.internal;

import java.util.Arrays;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.schuster.common.extendable.TaskBuilder;
import com.schuster.common.helper.Result;
import com.schuster.exception.UnsupportedException;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 6, 2015
 */
public class MouseCallBuilder implements TaskBuilder {
  private static final Logger LOGGER = LoggerFactory.getLogger(MouseCallBuilder.class);

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @throws UnsupportedException
   * @see com.schuster.common.extendable.TaskBuilder#buildTask(java.lang.String, java.lang.String[])
   * @since Aug 6, 2015
   */
  @Override
  public Callable<Result> buildTask(final String taskName, final String[] parameters)
      throws UnsupportedException {
    LOGGER.trace(String.format("Building Task %s with arguments: %s", taskName,
        Arrays.toString(parameters)));
    switch (taskName) {
      case MouseCall.CALL_NAME:
        return MouseCall.withParameters(parameters);
      default:
        throw new UnsupportedException(taskName);
    }
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.TaskBuilder#supports(java.lang.String)
   * @since Aug 6, 2015
   */
  @Override
  public boolean supports(final String taskName) {
    if (MouseCall.CALL_NAME.equals(taskName)) {
      return true;
    }
    return false;
  }
}
