/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.gui;

import com.schuster.common.extendable.BaseInformation;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class Information implements BaseInformation {

  /**
   * {@inheritDoc}
   * 
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.BaseInformation#getToolId()
   * @since Aug 6, 2015
   */
  @Override
  public String getToolId() {
    return "GuiPackage";
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.BaseInformation#getToolName()
   * @since Aug 5, 2015
   */
  @Override
  public String getToolName() {
    return "Gui Package";
  }
}
