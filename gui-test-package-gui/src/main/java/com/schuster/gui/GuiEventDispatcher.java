/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.gui;

import java.awt.AWTEvent;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.schuster.common.extendable.BaseInformation;
import com.schuster.common.extendable.EventDispatcher;
import com.schuster.exception.UnsupportedException;
import com.schuster.gui.internal.MouseCall;
import com.schuster.util.Pair;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class GuiEventDispatcher implements EventDispatcher {
  private static final Logger LOGGER = LoggerFactory.getLogger(GuiEventDispatcher.class);

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.EventDispatcher#canDispatch(java.awt.AWTEvent)
   * @since Aug 5, 2015
   */
  @Override
  public boolean canDispatch(final AWTEvent event) {
    boolean result = false;
    if (event instanceof MouseEvent) {
      result = this.canDispatch((MouseEvent) event);
    }
    LOGGER.trace(String.format("Can dispatch resulted in: %b", result));
    return result;
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.EventDispatcher#getBaseInfo()
   * @since Aug 5, 2015
   */
  @Override
  public BaseInformation getBaseInfo() {
    return null;
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @throws UnsupportedException
   * @see com.schuster.common.extendable.EventDispatcher#getCallInfo()
   * @since Aug 6, 2015
   */
  @Override
  public Pair<String, String[]> getCallInfo(final AWTEvent event) throws UnsupportedException {
    Pair<String, String[]> call = null;
    if (event instanceof MouseEvent) {
      call = this.constructMouseCall((MouseEvent) event);
    } else {
      throw new UnsupportedException("Not able to get Call Info for Event");
    }
    LOGGER.debug(String.format("Returning Call information: Name: %s; Arguments: %s", call.i,
        Arrays.toString(call.ii)));
    return call;
  }

  private boolean canDispatch(final MouseEvent event) {
    switch (event.getButton()) {
      case MouseEvent.NOBUTTON:
        return false;
      case MouseEvent.BUTTON1:
        return true;
      case MouseEvent.BUTTON2:
        return true;
      case MouseEvent.BUTTON3:
        return true;
      default:
        return false;
    }
  }

  /**
   * @author nschuste
   * @version 1.0.0
   * @param event
   * @return
   * @since Aug 6, 2015
   */
  private Pair<String, String[]> constructMouseCall(final MouseEvent event) {
    final MouseCall call = MouseCall.constructFromMouseEvent(event);
    return new Pair(call.getId(), call.getStringParameters());
  }
}
