/**
 * Copyright 2015 Nigel Schuster.
 */


package com.schuster.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.schuster.common.extendable.BaseInformation;
import com.schuster.common.extendable.EventDispatcher;
import com.schuster.common.extendable.Registry;
import com.schuster.common.extendable.TaskBuilder;
import com.schuster.common.registries.AbstractRegistry;
import com.schuster.gui.internal.MouseCallBuilder;

/**
 * @author nschuste
 * @version 1.0.0
 * @since Aug 5, 2015
 */
public class RegistryController implements Registry {
  private static final Logger LOGGER = LoggerFactory.getLogger(RegistryController.class);

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.Registry#getInfo()
   * @since Aug 5, 2015
   */
  @Override
  public BaseInformation getInfo() {
    return null;
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.Registry#register(com.schuster.common.registries.AbstractRegistry)
   * @since Aug 5, 2015
   */
  @Override
  public void registerDispatcher(final AbstractRegistry<EventDispatcher> dispatcherRegistry) {
    LOGGER.debug("Registering Dispatcher(s)");
    dispatcherRegistry.register(new GuiEventDispatcher());
  }

  /**
   * {@inheritDoc}
   *
   * @author nschuste
   * @version 1.0.0
   * @see com.schuster.common.extendable.Registry#registerTask(com.schuster.common.registries.AbstractRegistry)
   * @since Aug 6, 2015
   */
  @Override
  public void registerTask(final AbstractRegistry<TaskBuilder> taskRegistry) {
    LOGGER.debug("Registering TaskBuilders.");
    taskRegistry.register(new MouseCallBuilder());
  }

}
